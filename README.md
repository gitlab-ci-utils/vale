# Vale

A container image to run [Vale](https://vale.sh/) to lint prose, with pre-installed
rule packages and a custom GitLab Code Quality report.

## Container image

The container image derives from the [Vale image](https://hub.docker.com/r/jdkato/vale),
but with several additions:

- All [official Vale rule packages](https://vale.sh/hub/), as well as the
  custom [GCIU](https://gitlab.com/gitlab-ci-utils/vale-package-gciu) package
  are pre-installed and managed by version.
- A custom template to generate a GitLab Code Quality report is pre-installed.
  This is based on the
  [GitLab template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/.vale/vale-json.tmpl),
  but with several changes (for example, creating unique signatures for multiple
  findings per line, listing issues by column as well as line).

### Vale rule packages

The image includes the following official Vale packages:

- [`alex`](https://vale.sh/hub/alex/): Catch insensitive, inconsiderate writing.
- [`Google`](https://vale.sh/hub/google/): A Vale-compatible implementation of
  the Google Developer Documentation Style Guide.
- [`Joblint`](https://vale.sh/hub/joblint/): Test tech job posts for issues with
  sexism, culture, expectations, and recruiter fails.
- [`Microsoft`](https://vale.sh/hub/microsoft/): A Vale-compatible implementation
  of the Microsoft Writing Style Guide.
- [`proselint`](https://vale.sh/hub/proselint/): A collection of rules inspired by
  the open source proselint CLI tool.
- [`Readability`](https://vale.sh/hub/readability/): Vale-compatible implementations
  of many popular “readability” metrics.
- [`write-good`](https://vale.sh/hub/write-good/): Naive linter for English prose.
- [`Hugo`](https://github.com/errata-ai/Hugo) - Adds support for Hugo shortcodes
  and other non-standard markup.

These packages are managed by version to track changes. The actual packages installed,
with version numbers, are specified in the `/vale/vale-packages.txt` configuration file.

### Custom rule packages

Includes the following custom package:

- [`GCIU`](https://gitlab.com/gitlab-ci-utils/vale-package-gciu): A custom rule
  set and vocabulary for use with GitLab CI Utils projects. See the
  documentation for complete details.

### Using pre-installed packages

To use the pre-installed packages, use the `/vale/sync.sh` script as a
replacement to `vale sync`. This checks the current working directory for a
`.vale.ini` file. If found, it checks the packages specified in the `Packages`
property and creates a temporary configuration file (located at
`/vale/tmp/.vale.ini`), replacing any locally cached packages with the local
cache location. It then runs `vale sync` with that temporary configuration
file, installing packages from the local cache if available, otherwise
installing from the original URL.

For example, `.vale.ini`:

```ini
StylesPath = .gitlab/vale/styles
Packages = Google, \
  https://gitlab.com/gitlab-ci-utils/vale-package-gciu/-/releases/1.0.1/downloads/GCIU.zip
Vocab = GCIU

[*]
BasedOnStyles = Vale, Google, GCIU
```

Results in:

```sh
# ./vale/sync.sh
Found StylesPath at '.gitlab/vale/styles'
Found package 'Google' in package cache
Found package 'GCIU' in package cache
Installing Vale packages
 SUCCESS  Downloaded package 'Google'
 SUCCESS  Downloaded package 'GCIU'
Downloading packages [2/2] ██████████████████████████████████████████ 100% | 2s
```

Run `vale sync` instead to always install from the source locations.

## Install

Pull the image from the GitLab Container Registry with:

```sh
# docker pull registry.gitlab.com/gitlab-ci-utils/container-images/vale:latest
```

All available container image tags are available in the
`gitlab-ci-utils/container-images/vale` repository at
<https://gitlab.com/gitlab-ci-utils/container-images/vale/container_registry>.
The following container image tags are available:

- `X.Y.Z`: Tags for specific `vale` versions, for example `1.0.0`
- `latest`: Tag corresponding to the latest commit to the `main` branch of this
  repository

**Note:** All other image tags in this repository, and any images in the
`gitlab-ci-utils/container-images/vale/tmp` repository, are temporary images
used during the build process and may be deleted at any point.

## Usage

The following is an example job from a `.gitlab-ci.yml` file using this image.

```yml
lint_prose:
  image:
    name: registry.gitlab.com/gitlab-ci-utils/container-images/vale:latest
    entrypoint: ['']
  needs: []
  variables:
    VALE_REPORT: vale.json
  before_script:
    # Use pre-installed packages.
    - /vale/sync.sh
  script:
    # Run vale with the following options:
    #  - Output in GitLab Code Quality format.
    #  - Only lint markdown files.
    #  - Use `--no-exit` to prevent vale from exiting with a non-zero status
    #    code (consistent with Code Quality philosophy of only reporting).
    #  - Lint the current directory (and subdirectories).
    #  - Save the results to the file $VALE_REPORT.
    - vale --output="/vale/vale-glcq.tmpl" --glob="*.md" --no-exit ./ > $VALE_REPORT
  after_script:
    # Output a summary of the results (errors sorted by count)
    - jq -r '.[].check_name' vale.json | sort | uniq -c | sort -nr
  artifacts:
    paths:
      - $VALE_REPORT
    reports:
      codequality: $VALE_REPORT
```
