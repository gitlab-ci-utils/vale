#!/bin/sh

vale_config=".vale.ini"
vale_config_tmp_dir="/vale/tmp"
vale_config_tmp="$vale_config_tmp_dir/$vale_config"
vale_package_cache_dir="/vale/packages"
vale_package_list="/vale/vale-packages.txt"

append_with_comma() {
  if [ -n "$1" ]; then
    echo "$1, $2"
  else
    echo "$2"
  fi
}

# Create vale tmp folder and tmp config file.
mkdir -p "$vale_config_tmp_dir"
cp "$vale_config" "$vale_config_tmp"

# Get Vale configuration data
styles_path=$(grep '^StylesPath\s*=' "$vale_config_tmp" | sed -E 's/^StylesPath\s*=\s*(.*)\s*$/\1/')
echo "Found StylesPath at '$styles_path'"
mkdir -p "$styles_path"

# Update tmp config StylesPath with absolute path. Otherwise, will create folder
# relate to it's location and vale will fail to find the styles.
sed -i -E "s|$styles_path|$(realpath "$styles_path")|" "$vale_config_tmp"

# `Packages` may be split over multiple lines, so join any split lines in the tmp config file.
sed -i -e :a -e '/\\$/N; s/\\\n//; ta' "$vale_config_tmp"

# Get packages from config and check cache for each.
vale_packages=$(grep '^Packages\s*=' "$vale_config_tmp" | sed -E 's/^Packages\s*=\s*(.*)\s*$/\1/' | sed -E 's/[,[:space:]]+/ /g')
for package in $vale_packages; do
  is_http=$(echo "$package" | grep -E '^https://.*\.zip')
  # Since is_in_cache checks for the complete URL, checks package version as well.
  is_in_cache=$(grep "$package" "$vale_package_list")
  # If package is not found in cache, then use the original package name.
  package_cache="$package"
  # If package not https://*.zip and zip file found in cache, add to packages.
  if [ -z "$is_http" ] && [ -f "$vale_package_cache_dir/$package.zip" ]; then
    package_cache="$vale_package_cache_dir/$package.zip"
    echo "Found package '$package' in package cache"
  else
    # Get zip file name from package URL.
    http_package=$(echo "$package" | grep -E '^https://.*\.zip' | sed -E 's/^https:\/\/.+\/(.+\.zip)$/\1/')
    # If URL found in cached file list and zip file found in cache, then add to packages.
    if [ -n "$is_in_cache" ] && [ -f "$vale_package_cache_dir/$http_package" ]; then
      package_cache="$vale_package_cache_dir/$http_package"
      echo "Found package '$package' in package cache"
    else
      echo "Package '$package' not found in package cache"
    fi
  fi
  vale_packages_cache=$(append_with_comma "$vale_packages_cache" "$package_cache")
done
# Encode '/' in path for variable expansion in sed.
vale_packages_cache_encoded=$(echo "$vale_packages_cache" | sed -E 's/\//\\\//g')
# Replace packages in config with packages from cache, where found.
sed -i -E "s/^Packages\s*=.*$/Packages = $vale_packages_cache_encoded/" "$vale_config_tmp"

# Install vale pacakges from tmp config file. Vale constucts the folder based
# on package hierarchy, so need to actually run `vale sync`.
echo "Installing Vale packages"
vale --config="$vale_config_tmp" sync
