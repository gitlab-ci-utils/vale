# The initial build stage is only downloading and manipulating files, so use
# alpine since it has the necessary packages installed.
FROM alpine:3.21.3@sha256:a8560b36e8b8210634f77d9f7f9efd7ffa463e380b75e2e74aff4511df3ef88c as build

ARG VERSION

# Install the specified version of Vale.
RUN wget -q -O /tmp/vale.tar.gz https://github.com/errata-ai/vale/releases/download/v${VERSION}/vale_${VERSION}_Linux_64-bit.tar.gz && \
  tar -xzf /tmp/vale.tar.gz -C /bin/ && \
  rm -f /tmp/vale.tar.gz

# Install vale packages and scripts
COPY ./vale/ /vale/
RUN mkdir -p /vale/packages && \
  while read -r line; do wget -q -P /vale/packages "$line"; done < /vale/vale-packages.txt && \
  chmod +x /vale/sync.sh

FROM ubuntu:24.04@sha256:72297848456d5d37d1262630108ab308d3e9ec7ed1c3286a32fe09856619a782 as final

# Install latest version of OS packages.
# For Vale: asciidoctor python3-docutils
# jq: process JSON results
# wget: compatiblity with alpine to retrieve files
# hadolint ignore=DL3008
RUN apt-get update && \
  apt-get -y install --no-install-recommends asciidoctor python3-docutils jq wget && \
  rm -rf /var/lib/apt/lists/*

COPY --from=build /vale/ /vale/
COPY --from=build /bin/vale /bin/

ENTRYPOINT ["/bin/vale"]

LABEL org.opencontainers.image.licenses="MIT"
LABEL org.opencontainers.image.source="https://gitlab.com/gitlab-ci-utils/container-images/vale"
LABEL org.opencontainers.image.title="vale"
LABEL org.opencontainers.image.url="https://gitlab.com/gitlab-ci-utils/container-images/vale"
