# Changelog

## v3.3.6 (2025-03-01)

### Fixed

- Updated to `vale` [v3.9.6](https://github.com/errata-ai/vale/releases/tag/v3.9.6).

## v3.3.5 (2025-01-23)

### Fixed

- Updated to `vale` [v3.9.5](https://github.com/errata-ai/vale/releases/tag/v3.9.5).
- Updated base image to latest `ubuntu:24.04` with security patches.

## v3.3.4 (2025-01-23)

### Fixed

- Updated to `vale` [v3.9.4](https://github.com/errata-ai/vale/releases/tag/v3.9.4).

## v3.3.3 (2025-01-05)

### Fixed

- Updated to `GCIU` package [v1.0.6](https://gitlab.com/gitlab-ci-utils/vale-package-gciu/-/releases/1.0.6).

## v3.3.2 (2025-01-02)

### Fixed

- Updated to `vale` [v3.9.3](https://github.com/errata-ai/vale/releases/tag/v3.9.3).

## v3.3.1 (2024-12-25)

### Fixed

- Updated to `vale` [v3.9.2](https://github.com/errata-ai/vale/releases/tag/v3.9.2).
- Updated base image to latest `ubuntu:24.04` with security patches.

## v3.3.0 (2024-11-25)

### Changed

- Updated GitLab Code Quality template to add rule link to `description` if
  provided. (#11)

## v3.2.1 (2024-11-18)

### Fixed

- Updated to `vale` [v3.9.1](https://github.com/errata-ai/vale/releases/tag/v3.9.1).

## v3.2.0 (2024-11-07)

### Changed

- Updated to `vale` [v3.9.0](https://github.com/errata-ai/vale/releases/tag/v3.9.0).

### Fixed

- Updated to `alex` package [v0.2.3](https://github.com/errata-ai/alex/releases/tag/v0.2.3).
- Updated to `Google` package [v0.6.2](https://github.com/errata-ai/Google/releases/tag/v0.6.2).

## v3.1.0 (2024-10-21)

### Changed

- Updated to `vale` [v3.8.0](https://github.com/errata-ai/vale/releases/tag/v3.8.0).

### Fixed

- Updated base image to latest `ubuntu:24.04` with security patches.

## v3.0.2 (2024-10-16)

### Fixed

- Updated to `GCIU` package [v1.0.5](https://gitlab.com/gitlab-ci-utils/vale-package-gciu/-/releases/1.0.5).
- Updated base image to latest `ubuntu:24.04` with security patches.

## v3.0.1 (2024-09-28)

### Fixed

- Fixed CI pipeline to add correct OCI image `annotations`.

## v3.0.0 (2024-09-02)

### Changed

- BREAKING: Removed [`RedHat`](https://github.com/redhat-documentation/vale-at-red-hat)
  package from this container image. (#10)
  - The package can still be included with `Packages = RedHat` in a `.vale.ini`
    file, but the version is no longer managed in this image.

## v2.3.3 (2024-08-25)

### Fixed

- Updated to `vale` [v3.7.1](https://github.com/errata-ai/vale/releases/tag/v3.7.1).
- Updated to `RedHat` package [v548](https://github.com/redhat-documentation/vale-at-red-hat/compare/v547...v548).

## v2.3.2 (2024-08-18)

### Fixed

- Updated base image to latest `ubuntu:24.04`.
- Updated to `RedHat` package [v547](https://github.com/redhat-documentation/vale-at-red-hat/compare/v536...v547).

## v2.3.1 (2024-08-06)

### Fixed

- Updated to `RedHat` package [v536](https://github.com/redhat-documentation/vale-at-red-hat/compare/v519...v536).
- Updated base container image to `alpine:3.20.2`.

## v2.3.0 (2024-07-21)

### Changed

- Updated to `vale` [v3.7.0](https://github.com/errata-ai/vale/releases/tag/v3.7.0).

### Fixed

- Updated to `RedHat` package [v519](https://github.com/redhat-documentation/vale-at-red-hat/compare/v515...v519).

## v2.2.1 (2024-07-08)

### Fixed

- Updated to `vale` [v3.6.1](https://github.com/errata-ai/vale/releases/tag/v3.6.1).
- Updated to `RedHat` package [v515](https://github.com/redhat-documentation/vale-at-red-hat/compare/v512...v515).

## v2.2.0 (2024-07-01)

### Changed

- Updated to `RedHat` package [v512](https://github.com/redhat-documentation/vale-at-red-hat/compare/v502...v512).

## v2.1.1 (2024-06-21)

### Fixed

- Fix `GCIU` package update for this project's Vale config.

## v2.1.0 (2024-06-20)

### Changed

- Updated to `vale` [v3.6.0](https://github.com/errata-ai/vale/releases/tag/v3.6.0).
- Updated to `RedHat` package [v502](https://github.com/redhat-documentation/vale-at-red-hat/compare/v497...v502).

### Fixed

- Updated to `GCIU` package [v1.0.4](https://gitlab.com/gitlab-ci-utils/vale-package-gciu/-/releases/1.0.4).
- Updated base container image to `alpine:3.20.1`.

## v2.0.1 (2024-06-17)

### Fixed

- Updated to `RedHat` package [v497](https://github.com/redhat-documentation/vale-at-red-hat/compare/v493...v497).
- Updated to latest `ubuntu:24.04` image.

## v2.0.0 (2024-06-10)

### Changed

- BREAKING: Updated container base image to `ubuntu:24.04` for CGO
  compatibility, which was enabled in Vale v3.5.0. Image still contains `jq`
  and `wget` for maximum backwards compatibility with known uses.
- Updated to `vale` [v3.5.0](https://github.com/errata-ai/vale/releases/tag/v3.5.0).

### Fixed

- Updated to `alex` package [v0.2.2](https://github.com/errata-ai/alex/releases/tag/v0.2.2).
- Updated to `proselint` package [v0.3.4](https://github.com/errata-ai/proselint/releases/tag/v0.3.4).
- Updated to `RedHat` package [v494](https://github.com/redhat-documentation/vale-at-red-hat/compare/v493...v494).
- Updated to `write-good` package [v0.4.1](https://github.com/errata-ai/write-good/releases/tag/v0.4.1).

## v1.13.1 (2024-06-08)

### Fixed

- Updated to `RedHat` package [v493](https://github.com/redhat-documentation/vale-at-red-hat/compare/v490...v493).

## v1.13.0 (2024-05-29)

### Changed

- Updated to `RedHat` package [v490](https://github.com/redhat-documentation/vale-at-red-hat/compare/v487...v490).

## v1.12.0 (2024-05-23)

### Changed

- Updated base container image to `alpine:3.20.0`.

## v1.11.1 (2024-05-21)

### Fixed

- Updated to `RedHat` package [v487](https://github.com/redhat-documentation/vale-at-red-hat/compare/v484...v487).

## v1.11.0 (2024-05-09)

### Changed

- Updated to `RedHat` package [v484](https://github.com/redhat-documentation/vale-at-red-hat/compare/v480...v484).

## v1.10.0 (2024-05-05)

### Changed

- Updated to `RedHat` package [v480](https://github.com/redhat-documentation/vale-at-red-hat/compare/v478...v480).

## v1.9.1 (2024-05-01)

### Fixed

- Updated to `vale` [v3.4.2](https://github.com/errata-ai/vale/releases/tag/v3.4.2).

## v1.9.0 (2024-04-30)

### Changed

- Updated to `RedHat` package [v478](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v478).

### Fixed

- Updated to `Google` package [v0.6.1](https://github.com/errata-ai/Google/releases/tag/v0.6.1).
- Updated to `Microsoft` package [v0.14.1](https://github.com/errata-ai/Microsoft/releases/tag/v0.14.1).

## v1.8.0 (2024-04-23)

### Changed

- Updated to `RedHat` package [v477](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v477).

## v1.7.0 (2024-04-17)

### Changed

- Updated to `RedHat` package [v467](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v467).

## v1.6.0 (2024-04-08)

### Changed

- Updated to `vale` [v3.4.1](https://github.com/errata-ai/vale/releases/tag/v3.4.1).
- Updated to `RedHat` package [v458](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v458).

### Miscellaneous

- Updated CI pipeline to include security scans.

## v1.5.1 (2024-03-31)

### Fixed

- Updated to `RedHat` package [v456](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v456).

## v1.5.0 (2024-03-23)

### Changed

- Updated to `Google` package [v0.6.0](https://github.com/errata-ai/Google/releases/tag/v0.6.0).
- Updated to `Microsoft` package [v0.14.0](https://github.com/errata-ai/Microsoft/releases/tag/v0.14.0).
- Updated to `Hugo` package [v0.3.0](https://github.com/errata-ai/Hugo/releases/tag/v0.3.0).

### Fixed

- Updated to `vale` [v3.3.1](https://github.com/errata-ai/vale/releases/tag/v3.3.1).
- Updated to `RedHat` package [v451](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v451).

## v1.4.2 (2024-03-20)

### Fixed

- Updated to `RedHat` package [v450](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v450).

## v1.4.1 (2024-03-19)

### Fixed

- Updated to `RedHat` package [v447](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v447).

## v1.4.0 (2024-03-12)

### Changed

- Updated to `vale` [v3.3.0](https://github.com/errata-ai/vale/releases/tag/v3.3.0).

### Fixed

- Fixed issue with `/vale/sync.sh` incorrectly handling a relative `StylesPath`
  folder.
- Updated to `RedHat` package [v444](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v444).

## v1.3.3 (2024-03-06)

### Fixed

- Updated to `RedHat` package [v442](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v442).

## v1.3.2 (2024-03-06)

### Fixed

- Updated to `vale` [v3.2.2](https://github.com/errata-ai/vale/releases/tag/v3.2.2).
- Updated to `RedHat` package [v440](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v440).

## v1.3.1 (2024-03-04)

### Fixed

- Updated to `vale` [v3.2.1](https://github.com/errata-ai/vale/releases/tag/v3.2.1).
- Updated to `RedHat` package [v438](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v438).

## v1.3.0 (2024-02-29)

### Changed

- Updated to `vale` [v3.2.0](https://github.com/errata-ai/vale/releases/tag/v3.2.0).
- Updated to `RedHat` package [v437](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v437).

## v1.2.0 (2024-02-19)

### Changed

- Updated to `Google` package [v0.5.0](https://github.com/errata-ai/Google/releases/tag/v0.5.0).
- Updated to `Microsoft` package [v0.13.0](https://github.com/errata-ai/Microsoft/releases/tag/v0.13.0).

## v1.1.0 (2024-02-18)

### Changed

- Updated to `vale` [v3.1.0](https://github.com/errata-ai/vale/releases/tag/v3.1.0).

### Miscellaneous

- Updated Renovate config to use use new [presets](https://gitlab.com/gitlab-ci-utils/renovate-config)
  project. (#8)

## v1.0.7 (2024-02-14)

### Fixed

- Updated to `RedHat` package [v433](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v433).

## v1.0.6 (2024-02-10)

### Fixed

- Updated to `RedHat` package [v432](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v432).

## v1.0.5 (2024-02-01)

### Fixed

- Updated to `vale` [v3.0.7](https://github.com/errata-ai/vale/releases/tag/v3.0.7).
  Also updated `/vale/sync.sh` to create StylesPath folder if it does not exist
  due to changes in where `vale sync` installs packages.
- Updated Renovate rules for Vale packages to also update `.vale.ini` files.
  (#7)
- Updated to `RedHat` package [v431](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v431).

### Miscellaneous

- Updated container image build to install `vale` binary directly from GitHub
  release since the `jdkato/vale` image is not reliably published. (#5)

## v1.0.4 (2024-01-25)

### Fixed

- Updated to `RedHat` package [v430](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v430).

## v1.0.3 (2024-01-23)

### Fixed

- Updated to `GCIU` package [v1.0.2](https://gitlab.com/gitlab-ci-utils/vale-package-gciu/-/releases/1.0.2).
- Updated to `RedHat` package [v426](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v426).

## v1.0.2 (2024-01-21)

### Fixed

- Updated to `RedHat` package [v425](https://github.com/redhat-documentation/vale-at-red-hat/releases/tag/v425).

## v1.0.1 (2024-01-17)

### Fixed

- Updated to `vale` [v3.0.5](https://github.com/errata-ai/vale/releases/tag/v3.0.5).
- Updated to `Hugo` package [v0.2.0](https://github.com/errata-ai/Hugo/releases/tag/v0.2.0).

### Miscellaneous

- Updated CI pipeline to use `lint_prose` job for the image test.

## v1.0.0 (2024-01-14)

### Changed

- BREAKING: Update to Vale v3.0.4.
- BREAKING: Updated image to cache the original zip files, and `/vale/sync.sh`
  to check the cache and run `vale sync` using the cache where available and
  otherwise load from the source. (#6)
- Added [GCIU](https://gitlab.com/gitlab-ci-utils/vale-package-gciu) package
  to pre-installed packages, replacing the local `technical-readability`
  rules. (#6)

### Miscellaneous

- Updated Renovate config to manage GitLab and GitHub Vale packages. (#6)

## v0.5.0 (2024-01-05)

Initial release
